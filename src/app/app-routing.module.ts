import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContenidoComponent } from './shared/pages/contenido/contenido.component';
import { TemarioComponent } from './shared/pages/temario/temario.component';
import { PrivacidadComponent } from './shared/pages/privacidad/privacidad.component';


const routes: Routes = [

  {path: '', component: ContenidoComponent},
  {path: 'temario', component: TemarioComponent},
  {path: 'privacidad', component: PrivacidadComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
