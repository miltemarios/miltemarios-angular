import { Component, OnInit } from '@angular/core';
import { InfoPaginaService } from '../../../services/info-pagina.service';

import { infoTemario } from '../../../interfaces/interface';

@Component({
  selector: 'app-contenido',
  templateUrl: './contenido.component.html',
  styleUrls: ['./contenido.component.css']
})
export class ContenidoComponent implements OnInit {

  listaTemarios: infoTemario[];

  constructor(public infoServicio: InfoPaginaService) {   }

  ngOnInit() {

     return this.infoServicio.cargarTemarios()
    .snapshotChanges().subscribe(item => {
      
      this.listaTemarios = [];
     
      item.forEach(element => {
        
        let x = element.payload.toJSON();
        
        x["$key"] = element.key;
        
        this.listaTemarios.push(x as infoTemario);        

      });
      console.log("Categorias en Web: ", this.listaTemarios);      
    });

   
  }

}
