export interface infoTemario {
  Categoria: number;
  Descripcion?: string;
  Imagen?: string;
  Nombre?: string;
  Precio?: number;
  Profesor?: string;
  Valoracion?: number;
}