import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


// FIREBASE
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database'; 

//ESTE INJECTABLE ES PARA QUE NO HAGA FALTA IMPORTAR EL SERVICIO EN APP.MODULE.TS
@Injectable({
  providedIn: 'root'
})

export class InfoPaginaService {

  //misCategorias: any[] = [];
  misTemarios: AngularFireList<any>;
  // selectCategoria: infoCategorias = new infoCategorias();
  // info: infoTemario = {};

  // constructor(private http: HttpClient) { 
  //     this.cargarCategorias();  
  // }
  constructor(private firebase: AngularFireDatabase) {

  }

  cargarTemarios()  {
    
      return this.misTemarios = this.firebase.list('Temarios');
      console.log("Categorias en Servicio: ", this.misTemarios);

      // this.http.get('https://temario1.firebaseio.com/Categorias.json').subscribe(
      
      // (resp: any[]) => { 
    
      //       this.misCategorias = resp;
      //       console.log("Categorias en Servicio: ", this.misCategorias);
      // });
  
  }

  

  // private cargarInfo(){
    
  //   this.http.get('https://temario1.firebaseio.com/Categorias/0001/Temarios.json').subscribe((resp: infoTemario) => { 
  //     console.log(resp[0].Descripcion); 
  //   });

  // }

}
